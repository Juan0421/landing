<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package dazzling
 */
?>
                </div><!-- close .row -->
            </div><!-- close .container -->
        </div><!-- close .site-content -->

	<div id="footer-area">
		<div class="container footer-inner">
			<?php get_sidebar( 'footer' ); ?>
		</div>

		<footer id="colophonn" class="site-footer" role="contentinfo">
			<div class="site-info container">
			<div class="col-md-2" style="text-align: right; margin-top: 15px;">
				<img src="./wp-content/uploads/twitter.png">
				<img src="./wp-content/uploads/arrow.png">
			</div>
				<div class="copyright col-md-10" style="text-align: left;">
					<p style="color: #fff; font-size: 15px;">gsrthemes9: Liva - Responsive Multipurpose Professional Template. Create Outstanding Website in Minutes!</p>
					<p style="color: #fff;">.9 days ago .reply .retweet .favorite</p>
				</div>	
			</div><!-- .site-info -->
			<div class="scroll-to-top"><i class="fa fa-angle-up"></i></div><!-- .scroll-to-top -->
		</footer><!-- #colophon -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info container">
				<div class="copyright col-md-12" style="text-align: center;">
					<p>Copyright © 2013 ainexhost.com. All rights reserved.  Terms of Use | Privacy Policy</p>
				</div>
			</div><!-- .site-info -->
			<div class="scroll-to-top"><i class="fa fa-angle-up"></i></div><!-- .scroll-to-top -->
		</footer><!-- #colophon -->
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>